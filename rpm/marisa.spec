#
# spec file for package marisa
#
# Copyright (c) 2015 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#



Name:           marisa
Version:        0.2.5
Release:        0
Summary:        Matching Algorithm with Recursively Implemented StorAge
License:        LGPL-2.1+ or BSD-2-Clause
Group:          System/I18n/Japanese
Url:            https://code.google.com/p/marisa-trie/
Source:         https://marisa-trie.googlecode.com/files/%{name}-%{version}.tar.gz
Source99:       baselibs.conf
BuildRequires:  gcc-c++
BuildRequires:  pkgconfig
BuildRequires:	autoconf automake
BuildRequires:  swig
Provides:       marisa-trie = %{version}
Obsoletes:      marisa-trie < %{version}
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
Matching Algorithm with Recursively Implemented StorAge (MARISA) is a
static and space-efficient trie data structure. And libmarisa is a C++
library to provide an implementation of MARISA. Also, the package of
libmarisa contains a set of command line tools for building and
operating a MARISA-based dictionary.

A MARISA-based dictionary supports not only lookup but also reverse
lookup, common prefix search and predictive search.

%package -n libmarisa0
Summary:        Matching Algorithm with Recursively Implemented StorAge
Group:          System/Libraries

%description -n libmarisa0
The libmarisa0 package contains runtime libraries for marisa.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n %{name}-%{version}/%{name}

%build
autoreconf -fvi

%configure
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}%{_libdir}/libmarisa.{la,a}

%post -n libmarisa0 -p /sbin/ldconfig

%postun -n libmarisa0 -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_bindir}/%{name}-benchmark
%{_bindir}/%{name}-build
%{_bindir}/%{name}-common-prefix-search
%{_bindir}/%{name}-dump
%{_bindir}/%{name}-lookup
%{_bindir}/%{name}-predictive-search
%{_bindir}/%{name}-reverse-lookup

%files -n libmarisa0
%defattr(-,root,root)
%{_libdir}/libmarisa.so.0
%{_libdir}/libmarisa.so.0.0.0

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_includedir}/%{name}.h
%{_libdir}/libmarisa.so
%{_libdir}/pkgconfig/%{name}.pc

%changelog

